package com.grupoz10g13.electrostation.modelo;

import com.grupoz10g13.electrostation.R;


public enum TipoLocalizacion {
    AEROPUERTO("Aeropuerto", R.drawable.aeropuerto),
    VIA_PUBLICA("Vía pública", R.drawable.carretera),
    CONCESIONARIO("Concesionario", R.drawable.concesionario),
    ESTACION("Estacion de servicio", R.drawable.estacion),
    HOTEL("Hotel", R.drawable.hotel),
    MALL("Centro comercial", R.drawable.mall),
    PARKING("Parqueadero", R.drawable.parking),
    RESTAURANTE("Restaurante", R.drawable.restaurante),
    TALLLER("Taller de vehículos", R.drawable.taller),
    TIENDA("Tienda", R.drawable.tienda);

    private final String texto;
    private final int recurso;

    TipoLocalizacion(String texto, int recurso) {
        this.texto = texto;
        this.recurso = recurso;
    }

    public String getTexto() {
        return texto;
    }

    public int getRecurso() {
        return recurso;
    }

    public static String[] getNombres() {
        String[] resultado = new String[TipoLocalizacion.values().length];
        for (TipoLocalizacion tipo : TipoLocalizacion.values()) {
            resultado[tipo.ordinal()] = tipo.texto;
        }
        return resultado;
    }
}