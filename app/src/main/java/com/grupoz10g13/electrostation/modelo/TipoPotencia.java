package com.grupoz10g13.electrostation.modelo;

import com.grupoz10g13.electrostation.R;

public enum TipoPotencia {
    LENTA("Lenta", R.drawable.potencia1),
    SEMIRAPIDA ("Semirapida", R.drawable.potencia2),
    RAPIDA("Rápida", R.drawable.potencia3),
    ULTRARAPIDA("Ultra Rápida", R.drawable.potencia4);

    private final  String texto;
    private final int recurso;

    TipoPotencia(String texto, int recurso) {
        this.texto = texto;
        this.recurso = recurso;
    }

    public String getTexto() {
        return texto;
    }

    public int getRecurso() {
        return recurso;
    }

    public static String[] getNombres() {
        String[] resultado = new String[TipoPotencia.values().length];
        for (TipoPotencia tipo : TipoPotencia.values()) {
            resultado[tipo.ordinal()] = tipo.texto;
        }
        return resultado;
    }
}