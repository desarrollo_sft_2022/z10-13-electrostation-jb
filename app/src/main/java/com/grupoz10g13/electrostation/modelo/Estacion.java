package com.grupoz10g13.electrostation.modelo;

public class Estacion {
    private String nombre;
    private String direccion;
    private long telefono;
    private String url;
    private float costoRecarga;
    private float costoEstacionamiento;
    private String horario;
    private String serviciosAdicionales;
    private String indicaciones;
    private String comoFunciona;
    private int espacios;//Espacios de recarga habilitados
    private String foto;
    private float valoracion;
    private GeoPunto posicion;
    private TipoLocalizacion lugar;
    private TipoConector conector;

    //Constructor
    public Estacion(String nombre, String direccion, long telefono, String url,
                    float costoRecarga, float costoEstacionamiento, String horario,
                    String serviciosAdicionales, String indicaciones, String comoFunciona,
                    int espacios, String foto, float valoracion, double longitud, double latitud,
                    TipoLocalizacion lugar, TipoConector conector) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.url = url;
        this.costoRecarga = costoRecarga;
        this.costoEstacionamiento = costoEstacionamiento;
        this.horario = horario;
        this.serviciosAdicionales = serviciosAdicionales;
        this.indicaciones = indicaciones;
        this.comoFunciona = comoFunciona;
        this.espacios = espacios;
        this.foto = foto;
        this.valoracion = valoracion;
        posicion = new GeoPunto(longitud,latitud);
        this.lugar = lugar;
        this.conector = conector;
    }

    public Estacion(){
        //Sobrecarga de la clase
        nombre = "1245673";
        posicion = new GeoPunto(0.0, 0.0);
        lugar = TipoLocalizacion.VIA_PUBLICA;
        conector = TipoConector.CCS1;
    }

    //Getters & Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public float getCostoRecarga() {
        return costoRecarga;
    }

    public void setCostoRecarga(float costoRecarga) {
        this.costoRecarga = costoRecarga;
    }

    public float getCostoEstacionamiento() {
        return costoEstacionamiento;
    }

    public void setCostoEstacionamiento(float costoEstacionamiento) {
        this.costoEstacionamiento = costoEstacionamiento;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getServiciosAdicionales() {
        return serviciosAdicionales;
    }

    public void setServiciosAdicionales(String serviciosAdicionales) {
        this.serviciosAdicionales = serviciosAdicionales;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    public String getComoFunciona() {
        return comoFunciona;
    }

    public void setComoFunciona(String comoFunciona) {
        this.comoFunciona = comoFunciona;
    }

    public int getEspacios() {
        return espacios;
    }

    public void setEspacios(int espacios) {
        this.espacios = espacios;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public float getValoracion() {
        return valoracion;
    }

    public void setValoracion(float valoracion) {
        this.valoracion = valoracion;
    }

    public GeoPunto getPosicion() {
        return posicion;
    }

    public void setPosicion(GeoPunto posicion) {
        this.posicion = posicion;
    }

    public TipoLocalizacion getLugar() {
        return lugar;
    }

    public void setLugar(TipoLocalizacion lugar) {
        this.lugar = lugar;
    }

    public TipoConector getConector() {
        return conector;
    }

    public void setConector(TipoConector conector) {
        this.conector = conector;
    }

    @Override
    public String toString() {
        return "Estacion{" +
                "nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                ", telefono=" + telefono +
                ", url='" + url + '\'' +
                ", costoRecarga=" + costoRecarga +
                ", costoEstacionamiento=" + costoEstacionamiento +
                ", horario='" + horario + '\'' +
                ", serviciosAdicionales='" + serviciosAdicionales + '\'' +
                ", indicaciones='" + indicaciones + '\'' +
                ", comoFunciona='" + comoFunciona + '\'' +
                ", espacios=" + espacios +
                ", foto='" + foto + '\'' +
                ", valoracion=" + valoracion +
                ", posicion=" + posicion +
                ", lugar=" + lugar +
                ", conector=" + conector +
                '}';
    }
}
