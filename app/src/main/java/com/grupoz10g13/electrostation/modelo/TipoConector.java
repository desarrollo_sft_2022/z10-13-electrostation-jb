package com.grupoz10g13.electrostation.modelo;

import com.grupoz10g13.electrostation.R;

public enum TipoConector {
    CCS1("CCS tipo 1", R.drawable.ccs1),
    CCS2("CCS tipo 2", R.drawable.ccs1),
    CHADEMO ("CHAdeMO", R.drawable.chademo),
    GBT1 ("GBT Tipo 1", R.drawable.gbt1),
    GBT2 ("GBT Tipo 2", R.drawable.gbt1),
    TESLA ("Tesla", R.drawable.tesla);

    private final  String texto;
    private final int recurso;

    TipoConector(String texto, int recurso) {
        this.texto = texto;
        this.recurso = recurso;
    }

    public String getTexto() {
        return texto;
    }

    public int getRecurso() {
        return recurso;
    }

    public static String[] getNombres() {
        String[] resultado = new String[TipoConector.values().length];
        for (TipoConector tipo : TipoConector.values()) {
            resultado[tipo.ordinal()] = tipo.texto;
        }
        return resultado;
    }

}
