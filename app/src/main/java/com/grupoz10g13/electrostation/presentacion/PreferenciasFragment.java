package com.grupoz10g13.electrostation.presentacion;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.grupoz10g13.electrostation.R;

public class PreferenciasFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencias);
    }
}
