package com.grupoz10g13.electrostation.presentacion;

import android.database.Cursor;

import com.grupoz10g13.electrostation.datos.EstacionesBD;
import com.grupoz10g13.electrostation.datos.RepositorioLugares;
import com.grupoz10g13.electrostation.modelo.Estacion;

public class AdaptadorLugaresBD extends AdaptadorLugares{
    protected Cursor cursor;

    public AdaptadorLugaresBD(RepositorioLugares lugares, Cursor cursor) {
        super(lugares);
        this.cursor = cursor;
    }

    public Cursor getCursor() {
        return cursor;
    }

    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
    }

    public Estacion estacionPosicion(int posicion){
        cursor.moveToPosition(posicion);
        return EstacionesBD.extraeEstacion(cursor);
    }

    public int idPosicion(int posicion) {
        cursor.moveToPosition(posicion);
        if (cursor.getCount() > 0){
            return cursor.getInt(0);
        } else {
            return -1;
        }
    }
    public int posicionId(int id) {
        int pos = 0;
        while (pos<getItemCount() && idPosicion(pos)!=id) pos++;
        if (pos >= getItemCount()) return -1;
        else return pos;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
       Estacion estacion = estacionPosicion(position);
       holder.personalizaEstacion(estacion);
       holder.itemView.setTag(new Integer(position));
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }

}
