package com.grupoz10g13.electrostation.presentacion;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.FirebaseFirestore;
import com.grupoz10g13.electrostation.Aplicacion;
import com.grupoz10g13.electrostation.R;
import com.grupoz10g13.electrostation.casos_uso.CasosUsoActividades;
import com.grupoz10g13.electrostation.casos_uso.CasosUsoEstacion;
import com.grupoz10g13.electrostation.casos_uso.CasosUsoLocalizacion;
import com.grupoz10g13.electrostation.datos.EstacionesBD;
import com.grupoz10g13.electrostation.datos.RepositorioLugares;

public class MainActivity extends AppCompatActivity {

    private CasosUsoEstacion usoEstacion;
    private CasosUsoActividades usoActividades;
    private CasosUsoLocalizacion usoLocalizacion;

    private RecyclerView recyclerView;
    public EstacionesBD estaciones;
    public AdaptadorLugaresBD adaptadorLugares;

    static final int RESULTADO_PREFERENCIAS = 0;
    private static final int SOLICITUD_PERMISO_LOCALIZACION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adaptadorLugares = ((Aplicacion) getApplication()).adaptadorLugaresBD;
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adaptadorLugares);

        estaciones = ((Aplicacion) getApplication()).estaciones;
        usoEstacion = new CasosUsoEstacion(this,estaciones,adaptadorLugares);
        usoActividades = new CasosUsoActividades(this);

        adaptadorLugares.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int posicion = (Integer) view.getTag();//recyclerView.getChildAdapterPosition(view);
                usoEstacion.mostrar(posicion);
            }
        });

        usoLocalizacion = new CasosUsoLocalizacion(this,SOLICITUD_PERMISO_LOCALIZACION);
/*
        FirebaseFirestore firestoreDB_lugares = FirebaseFirestore.getInstance();
        for (int id = 0; id<adaptadorLugares.getItemCount(); id++) {
            Log.d("MAIN", "tamaño base datos ->" + adaptadorLugares.estacionPosicion(id));
            firestoreDB_lugares.collection("estaciones").add(adaptadorLugares.estacionPosicion(id));
            usoLocalizacion = new CasosUsoLocalizacion(this, SOLICITUD_PERMISO_LOCALIZACION);
        }*/

        //Barra de acciones
        Toolbar toolbar = findViewById(R.id.toolbar_Main);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout toolbarLayout = findViewById(R.id.toolbar_layout_Main);
        toolbar.setTitle(getTitle());

        //Boton flotante FAB circular
        FloatingActionButton fab = findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usoEstacion.nuevo();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_mapa){
            usoActividades.lanzarMapa();
            Log.d("Tag main","click a la opcion mapa");
            return true;
        }
        if (id == R.id.menu_buscar){
            lanzarVerEstacion(null);
            Log.d("Tag main","click a la opcion buscar");
            return true;
        }
        if (id == R.id.menu_filtro){
            Log.d("Tag main","click a la opcion filtrar");
            return true;
        }
        if (id == R.id.menu_usuario){
            usoActividades.lanzarUsuario();
            Log.d("Tag main","click a la opcion usuario");
            return true;
        }
        if (id == R.id.ajustes){
            usoActividades.lanzarPreferencias(RESULTADO_PREFERENCIAS);
            Log.d("Tag en Main","Click en la opcion ajustes");
            return true;
        }
        if (id == R.id.acercaDe){
            usoActividades.lanzarAcercaDe();
            Log.d("Tag main","click a la opcion acerca de");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void lanzarVerEstacion(View view){
        final EditText entrada = new EditText(this);
        entrada.setText("0");
        new AlertDialog.Builder(this)
                .setTitle("Ver Estación")
                .setMessage("Indicar su id:")
                .setView(entrada)
                .setPositiveButton("Ok", new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                int id = Integer.parseInt(entrada.getText().toString());
                                usoEstacion.mostrar(id);
                            }})
                .setNegativeButton("Cancelar", null)
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == SOLICITUD_PERMISO_LOCALIZACION
                && grantResults.length == 1
                && grantResults[0]== PackageManager.PERMISSION_GRANTED) {
                    usoLocalizacion.permisoConcedido();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULTADO_PREFERENCIAS) {
            adaptadorLugares.setCursor(estaciones.extraeCursor());
            adaptadorLugares.notifyDataSetChanged(); }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("tag MA", "onresume main ");
        usoLocalizacion.activar();
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d("tag MA", "onpause main ");
        usoLocalizacion.desactivar();
    }
}