package com.grupoz10g13.electrostation.presentacion;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.grupoz10g13.electrostation.R;

public class Splash extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        TextView textoInicio = findViewById(R.id.texto_inicio);
        ImageView imagenInicio = findViewById(R.id.logo_inicio);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent abrirApp = new Intent(Splash.this, LoginActivity.class);
                ActivityOptions activityOptions = ActivityOptions.makeCustomAnimation(Splash.this, com.firebase.ui.auth.R.anim.fui_slide_in_right, R.anim.transicion_vista_splash);
                startActivity(abrirApp,activityOptions.toBundle());
                finish();
            }
        },3500);
    }
}
