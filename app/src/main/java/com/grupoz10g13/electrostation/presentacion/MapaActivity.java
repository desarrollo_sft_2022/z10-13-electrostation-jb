package com.grupoz10g13.electrostation.presentacion;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.grupoz10g13.electrostation.Aplicacion;
import com.grupoz10g13.electrostation.R;
import com.grupoz10g13.electrostation.modelo.Estacion;
import com.grupoz10g13.electrostation.modelo.GeoPunto;

public class MapaActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mapa;
    private AdaptadorLugaresBD adaptadorLugaresBD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((R.layout.mapa));
        adaptadorLugaresBD = ((Aplicacion) getApplication()).adaptadorLugaresBD;

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;
        mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED){
            mapa.setMyLocationEnabled(true);
            mapa.getUiSettings().setZoomControlsEnabled(true);
            mapa.getUiSettings().setCompassEnabled(true);
            mapa.setOnInfoWindowClickListener(this);
        }

        if(adaptadorLugaresBD.getItemCount() > 0){
            GeoPunto punto = adaptadorLugaresBD.estacionPosicion(0).getPosicion();
            mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(punto.getLatitud(),punto.getLongitud()),15));// zoom del mapa de 2-21
        }

        for (int n=0; n<adaptadorLugaresBD.getItemCount(); n++){
            Estacion estacion = adaptadorLugaresBD.estacionPosicion(n);
            GeoPunto punto = estacion.getPosicion();
            if(punto != null && punto.getLatitud() != 0){
                Bitmap iGrande = BitmapFactory.decodeResource(
                        getResources(), estacion.getLugar().getRecurso());
                Bitmap icono =  Bitmap.createScaledBitmap(
                        iGrande, iGrande.getWidth()/10, iGrande.getHeight()/10,false);
                //Agrega marcadores al mapa de los lugares
                mapa.addMarker(new MarkerOptions().position(new LatLng(punto.getLatitud(),punto.getLongitud()))
                        .title(estacion.getNombre())
                        .snippet(estacion.getDireccion())
                        .icon(BitmapDescriptorFactory.fromBitmap(icono)));
            }
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        for (int pos=0; pos<adaptadorLugaresBD.getItemCount(); pos++){
            if (adaptadorLugaresBD.estacionPosicion(pos).getNombre().equals(marker.getTitle())){
                Intent intent = new Intent(this, VerEstacionActivity.class);
                intent.putExtra("pos", pos);
                startActivity(intent);
                break;
            }
        }
    }
}
