package com.grupoz10g13.electrostation.presentacion;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.grupoz10g13.electrostation.Aplicacion;
import com.grupoz10g13.electrostation.R;
import com.grupoz10g13.electrostation.casos_uso.CasosUsoEstacion;
import com.grupoz10g13.electrostation.datos.EstacionesBD;
import com.grupoz10g13.electrostation.datos.RepositorioLugares;
import com.grupoz10g13.electrostation.modelo.Estacion;

public class VerEstacionActivity extends AppCompatActivity {

    //private RepositorioLugares lugares;
    private EstacionesBD lugares;
    private AdaptadorLugaresBD adaptadorLugaresBD;
    private CasosUsoEstacion usoEstacion;

    private int pos, _id = -1;
    private Estacion estacion;
    private Uri uriUltimaFoto;
    final static int RESULTADO_EDITAR = 1;
    final static int RESULTADO_GALERIA = 2;
    final static int RESULTADO_FOTO = 3;
    private static final int SOLICITUD_PERMISO_LECTURA = 0;

    private TextView nombre, tipoLugar, tipoConector, direccion, telefono, url, costoRecarga,
            costoParqueo, espacios, horario, servicios, comoLLegar, comoFunciona;
    private ImageView logo_tipoLugar, logo_tipoConector, foto, galeria, camara, eliminar;
    private RatingBar valoracion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_estacion);

        lugares = ((Aplicacion) getApplication()).estaciones;
        adaptadorLugaresBD = ((Aplicacion)getApplication()).adaptadorLugaresBD;
        usoEstacion = new CasosUsoEstacion(this,lugares,adaptadorLugaresBD);

        Bundle extras = getIntent().getExtras();
        if(extras !=null) pos = extras.getInt("pos",0);
        else pos = 0;
        _id = adaptadorLugaresBD.idPosicion(pos);

        estacion = adaptadorLugaresBD.estacionPosicion(pos);//lugares.elemento(pos);

        foto = findViewById(R.id.foto);
        galeria = findViewById(R.id.galeria);
        camara = findViewById(R.id.camara);
        eliminar = findViewById(R.id.eliminar);

        actualizaVista();
        llamar();
        verWeb();
        abrirGaleria();
        tomarFotoCamara();
        eliminarFoto();
    }

    public void actualizaVista(){
        nombre = findViewById(R.id.nombre);
        nombre.setText((estacion.getNombre()));

        logo_tipoLugar =findViewById(R.id.icono_tipo_lugar);
        logo_tipoLugar.setImageResource(estacion.getLugar().getRecurso());

        tipoLugar = findViewById(R.id.tipo_lugar);
        tipoLugar.setText((estacion.getLugar().getTexto()));

        logo_tipoConector =findViewById(R.id.icono_tipo_conector);
        logo_tipoConector.setImageResource(estacion.getConector().getRecurso());

        tipoConector = findViewById(R.id.tipo_conector);
        tipoConector.setText((estacion.getConector().getTexto()));

        direccion = findViewById(R.id.direccion);
        direccion.setText(estacion.getDireccion());

        telefono = findViewById(R.id.telefono);
        telefono.setText(Long.toString((long) estacion.getTelefono()));

        url = findViewById(R.id.url);
        url.setText(estacion.getUrl());

        costoRecarga = findViewById(R.id.costo_recarga);
        costoRecarga.setText(Float.toString((float) estacion.getCostoRecarga())+" $/KWh");

        costoParqueo = findViewById(R.id.costo_parking);
        costoParqueo.setText(Float.toString((float) estacion.getCostoEstacionamiento())+" $/min");

        espacios = findViewById(R.id.espacios);
        espacios.setText(Integer.toString(estacion.getEspacios()));

        horario = findViewById(R.id.horario);
        horario.setText(estacion.getHorario());

        servicios = findViewById(R.id.servicios);
        servicios.setText(estacion.getServiciosAdicionales());

        comoLLegar = findViewById(R.id.como_llegar);
        comoLLegar.setText(estacion.getIndicaciones());

        comoFunciona = findViewById(R.id.como_funciona);
        comoFunciona.setText(estacion.getComoFunciona());

        valoracion = findViewById(R.id.valoracion);
        valoracion.setRating(estacion.getValoracion());
        valoracion.setOnRatingBarChangeListener(
                new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                        estacion.setValoracion(v);
                        usoEstacion.actualizaPosEstacion(pos, estacion);
                        pos = adaptadorLugaresBD.posicionId(_id);
                    }
                }
        );

        usoEstacion.visualizarFoto(estacion,foto);
    }

    public void llamar(){
        telefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usoEstacion.llamar(estacion);
            }
        });
    }

    public void verWeb(){
        url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usoEstacion.verWeb(estacion);
            }
        });
    }

    public void abrirGaleria(){
        galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usoEstacion.importarGaleria(RESULTADO_GALERIA);
            }
        });
    }

    public void tomarFotoCamara(){
        camara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uriUltimaFoto = usoEstacion.tomarFoto(RESULTADO_FOTO);
            }
        });
    }

    public void eliminarFoto(){
        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v,"Imagen borrada",Snackbar.LENGTH_LONG).show();
                usoEstacion.ponerFoto(pos,"",foto);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ver_estacion,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_compartir:
                usoEstacion.compartir(estacion);
                return true;
            case R.id.accion_llegar:
                usoEstacion.verMapa(estacion);
                return true;
            case R.id.accion_editar:
                usoEstacion.editar(pos,RESULTADO_EDITAR);
                return true;
            case R.id.accion_borrar:
                int id = adaptadorLugaresBD.idPosicion(pos);
                usoEstacion.borrar(id);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULTADO_EDITAR){
            estacion = lugares.elemento(_id);
            pos = adaptadorLugaresBD.posicionId(_id);
            actualizaVista();
            findViewById(R.id.scrollView1).invalidate();

        } else if (requestCode == RESULTADO_GALERIA) {
            if (resultCode == Activity.RESULT_OK) {
                usoEstacion.ponerFoto(pos, data.getDataString(), foto);
            } else {
                Toast.makeText(this, "Foto no cargada",Toast.LENGTH_LONG).show();
            }

        } else if (requestCode == RESULTADO_FOTO) {
            if (resultCode == Activity.RESULT_OK && uriUltimaFoto != null) {
                estacion.setFoto(uriUltimaFoto.toString());
                usoEstacion.ponerFoto(pos, estacion.getFoto(), foto);
            } else {
                Toast.makeText(this, "Error en captura", Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == SOLICITUD_PERMISO_LECTURA){
            if (grantResults.length== 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                usoEstacion.ponerFoto(pos, estacion.getFoto(), foto);
            } else {
                usoEstacion.ponerFoto(pos, "", foto);
            }
        }
    }
}
