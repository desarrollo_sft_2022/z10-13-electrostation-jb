package com.grupoz10g13.electrostation.presentacion;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.grupoz10g13.electrostation.Aplicacion;
import com.grupoz10g13.electrostation.R;
import com.grupoz10g13.electrostation.datos.RepositorioLugares;
import com.grupoz10g13.electrostation.modelo.Estacion;
import com.grupoz10g13.electrostation.modelo.GeoPunto;

public class AdaptadorLugares extends RecyclerView.Adapter<AdaptadorLugares.ViewHolder> {
   protected RepositorioLugares lugares;
   protected View.OnClickListener onClickListener;

    public AdaptadorLugares(RepositorioLugares lugares) {
        this.lugares = lugares;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView nombre, direccion, distancia, espacios;
        public ImageView icono;
        public RatingBar valoracion;

        public ViewHolder (View itemView){
            super((itemView));
            nombre = itemView.findViewById(R.id.nombre);
            direccion = itemView.findViewById(R.id.direccion);
            espacios = itemView.findViewById(R.id.espacios);
            distancia = itemView.findViewById(R.id.distancia);
            icono = itemView.findViewById(R.id.icono_lugar);
            valoracion = itemView.findViewById(R.id.valoracion);
        }

        public void personalizaEstacion (Estacion estacion){
            nombre.setText(estacion.getNombre());
            direccion.setText(estacion.getDireccion());
            espacios.setText("Espacios: "+ estacion.getEspacios());
            int id = R.drawable.ev_station;
            switch (estacion.getLugar()){
                case MALL: id = R.drawable.mall; break;
                case HOTEL: id = R.drawable.hotel; break;
                case TIENDA: id = R.drawable.tienda; break;
                case PARKING: id = R.drawable.parking; break;
                case TALLLER: id = R.drawable.taller; break;
                case ESTACION: id = R.drawable.estacion; break;
                case AEROPUERTO: id = R.drawable.aeropuerto; break;
                case RESTAURANTE: id = R.drawable.restaurante; break;
                case VIA_PUBLICA: id = R.drawable.carretera; break;
                case CONCESIONARIO: id = R.drawable.concesionario; break;
            }
            icono.setImageResource(id);
            icono.setScaleType(ImageView.ScaleType.FIT_END);
            valoracion.setRating(estacion.getValoracion());

            GeoPunto pos=((Aplicacion) itemView.getContext().getApplicationContext()).posicionActual;
            if (pos.equals(GeoPunto.SIN_POSICION) || estacion.getPosicion().equals(GeoPunto.SIN_POSICION)) {
                distancia.setText("... Km");
            } else {
                int d=(int) pos.distancia(estacion.getPosicion());
                if (d < 2000) {
                    distancia.setText(d + " m");
                }else{
                    distancia.setText(d / 1000 + " Km");
                }
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflamos la vista desde el xml
        View laVIsta_un_elemento = LayoutInflater.from(parent.getContext()).inflate(R.layout.elemento_lista, parent, false);
        laVIsta_un_elemento.setOnClickListener(onClickListener);
        return new ViewHolder(laVIsta_un_elemento);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
            Estacion estacion = lugares.elemento(position);
            holder.personalizaEstacion(estacion);
    }

    @Override
    public int getItemCount() {
        return lugares.tamanio();
    }
}
