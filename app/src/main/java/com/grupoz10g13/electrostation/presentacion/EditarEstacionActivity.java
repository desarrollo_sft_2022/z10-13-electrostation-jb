package com.grupoz10g13.electrostation.presentacion;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.grupoz10g13.electrostation.Aplicacion;
import com.grupoz10g13.electrostation.R;
import com.grupoz10g13.electrostation.casos_uso.CasosUsoEstacion;
import com.grupoz10g13.electrostation.datos.EstacionesBD;
import com.grupoz10g13.electrostation.datos.RepositorioLugares;
import com.grupoz10g13.electrostation.modelo.Estacion;
import com.grupoz10g13.electrostation.modelo.TipoConector;
import com.grupoz10g13.electrostation.modelo.TipoLocalizacion;

public class EditarEstacionActivity extends AppCompatActivity {

    private CasosUsoEstacion usoEstacion;
    private int pos, _id;
    private Estacion estacion;

    private EditText nombre, direccion, telefono, url, costoRecarga, costoParqueo,
            espacios, horario, servicios, comoLLegar, comoFunciona;
    private Spinner tipoLugar, tipoConector;

    private Toast msnToast;

    private EstacionesBD lugares;
    private AdaptadorLugaresBD adaptadorLugaresBD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editar_estacion);

        lugares = ((Aplicacion) getApplication()).estaciones;
        adaptadorLugaresBD =((Aplicacion)getApplication()).adaptadorLugaresBD;
        usoEstacion = new CasosUsoEstacion(this, lugares,adaptadorLugaresBD);

        Bundle extras = getIntent().getExtras();
        pos = extras.getInt("pos", -1);
        _id = extras.getInt("_id", -1);
        if (_id!=-1){
            setTitle("Crear nueva estación");
            estacion = lugares.elemento(_id);
        }
        else estacion = adaptadorLugaresBD.estacionPosicion(pos);//lugares.elemento(pos);

        actualizaVista();
    }

    public void actualizaVista(){

        nombre = findViewById(R.id.nombre);
        Log.d("Nombre", estacion.getNombre().toString());
        if (estacion.getNombre().equals("1245673")){
            Log.d("Nombre", "if");
            nombre.setText("");
        } else {
            nombre.setText((estacion.getNombre()));
            Log.d("Nombre", "else");
        }

        direccion = findViewById(R.id.direccion);
        direccion.setText(estacion.getDireccion());

        telefono = findViewById(R.id.telefono);
        telefono.setText(Long.toString((long) estacion.getTelefono()));

        url = findViewById(R.id.url);
        url.setText(estacion.getUrl());

        costoRecarga = findViewById(R.id.costoRecarga);
        costoRecarga.setText(Float.toString((float) estacion.getCostoRecarga()));

        costoParqueo = findViewById(R.id.costoParqueo);
        costoParqueo.setText(Float.toString((float) estacion.getCostoEstacionamiento()));

        espacios = findViewById(R.id.espacios);
        espacios.setText(Integer.toString(estacion.getEspacios()));

        horario = findViewById(R.id.horario);
        horario.setText(estacion.getHorario());

        servicios = findViewById(R.id.servicios);
        servicios.setText(estacion.getServiciosAdicionales());

        comoLLegar = findViewById(R.id.indicaciones);
        comoLLegar.setText(estacion.getIndicaciones());

        comoFunciona = findViewById(R.id.instrucciones);
        comoFunciona.setText(estacion.getComoFunciona());

        tipoLugar = findViewById(R.id.tipoLocalizacion);
        ArrayAdapter<String> adaptadorLugar = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TipoLocalizacion.getNombres());
        adaptadorLugar.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tipoLugar.setAdapter(adaptadorLugar);
        tipoLugar.setSelection(estacion.getLugar().ordinal());

        tipoConector = findViewById(R.id.tipoConector);
        ArrayAdapter<String> adaptadorConector = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, TipoConector.getNombres());
        adaptadorConector.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tipoConector.setAdapter(adaptadorConector);
        tipoConector.setSelection(estacion.getConector().ordinal());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editar_estacion,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_guardar:
                estacion.setNombre(nombre.getText().toString());
                estacion.setDireccion(direccion.getText().toString());
                estacion.setTelefono(Long.parseLong(telefono.getText().toString()));
                estacion.setUrl(url.getText().toString());
                estacion.setCostoRecarga(Float.parseFloat(costoRecarga.getText().toString()));
                estacion.setCostoEstacionamiento(Float.parseFloat(costoParqueo.getText().toString()));
                estacion.setEspacios(Integer.parseInt(espacios.getText().toString()));
                estacion.setHorario(horario.getText().toString());
                estacion.setServiciosAdicionales(servicios.getText().toString());
                estacion.setIndicaciones(comoLLegar.getText().toString());
                estacion.setComoFunciona(comoFunciona.getText().toString());
                estacion.setLugar(TipoLocalizacion.values()[tipoLugar.getSelectedItemPosition()]);
                estacion.setConector(TipoConector.values()[tipoConector.getSelectedItemPosition()]);

                if (_id==-1)_id = adaptadorLugaresBD.idPosicion(pos);
                usoEstacion.guardar(_id, estacion);

                msnToast = Toast.makeText(this,"Cambios guardados", Toast.LENGTH_LONG);
                msnToast.setGravity(Gravity.BOTTOM,0,30);
                msnToast.show();
                finish();
                return true;
            case R.id.accion_cancelar:

                msnToast = Toast.makeText(this,"No hay cambios", Toast.LENGTH_LONG);
                msnToast.setGravity(Gravity.BOTTOM,0,30);
                msnToast.show();
                if (_id!=-1) lugares.borrar(_id);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (_id != -1 & direccion.getText().toString().isEmpty()){
            Log.d("tag", "Borra datos vacios " + estacion.toString());
            lugares.borrar(_id);
        } else {
            Log.d("tag", "No borra ");
        }
    }
}
