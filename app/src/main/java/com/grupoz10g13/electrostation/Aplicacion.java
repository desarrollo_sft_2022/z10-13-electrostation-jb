package com.grupoz10g13.electrostation;

import android.app.Application;

import com.grupoz10g13.electrostation.datos.EstacionesBD;
import com.grupoz10g13.electrostation.datos.EstacionesLista;
import com.grupoz10g13.electrostation.datos.RepositorioLugares;
import com.grupoz10g13.electrostation.modelo.GeoPunto;
import com.grupoz10g13.electrostation.presentacion.AdaptadorLugares;
import com.grupoz10g13.electrostation.presentacion.AdaptadorLugaresBD;

public class Aplicacion extends Application {

    //public RepositorioLugares estaciones = new EstacionesLista();
    //public AdaptadorLugares adaptador = new AdaptadorLugares(estaciones);
    public GeoPunto posicionActual = new GeoPunto(0.0, 0.0);

    public EstacionesBD estaciones;
    public AdaptadorLugaresBD adaptadorLugaresBD;

    @Override
    public void onCreate() {
        super.onCreate();
        estaciones = new EstacionesBD(this);
        adaptadorLugaresBD = new AdaptadorLugaresBD(estaciones, estaciones.extraeCursor());
    }

    public RepositorioLugares getLugares() {
        return estaciones;
    }

}
