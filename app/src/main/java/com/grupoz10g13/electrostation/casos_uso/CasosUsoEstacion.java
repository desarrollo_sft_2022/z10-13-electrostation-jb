package com.grupoz10g13.electrostation.casos_uso;

import static com.grupoz10g13.electrostation.casos_uso.CasosUsoLocalizacion.solicitarPermiso;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.grupoz10g13.electrostation.Aplicacion;
import com.grupoz10g13.electrostation.R;
import com.grupoz10g13.electrostation.datos.EstacionesBD;
import com.grupoz10g13.electrostation.modelo.Estacion;
import com.grupoz10g13.electrostation.modelo.GeoPunto;
import com.grupoz10g13.electrostation.presentacion.AdaptadorLugaresBD;
import com.grupoz10g13.electrostation.presentacion.EditarEstacionActivity;
import com.grupoz10g13.electrostation.presentacion.VerEstacionActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class CasosUsoEstacion {
    private Activity actividad;
    private EstacionesBD lugares;
    private AdaptadorLugaresBD adaptadorLugaresBD;
    private static final int SOLICITUD_PERMISO_LECTURA = 0;

    public CasosUsoEstacion(Activity actividad, EstacionesBD lugares, AdaptadorLugaresBD adaptadorLugaresBD) {
        this.actividad = actividad;
        this.lugares = lugares;
        this.adaptadorLugaresBD = adaptadorLugaresBD;
    }

    public void actualizaPosEstacion(int pos, Estacion estacion) {
        int id = adaptadorLugaresBD.idPosicion(pos);
        guardar(id, estacion);
    }

    public void mostrar(int pos) {
        Intent mostrar = new Intent(actividad, VerEstacionActivity.class);
        mostrar.putExtra("pos", pos);
        actividad.startActivity(mostrar);
    }

    public void borrar (final int id){
        new AlertDialog.Builder(actividad)
                .setTitle("Eliminar estación")
                .setMessage("¿Está seguro de eliminar esta estación?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        lugares.borrar(id);
                        adaptadorLugaresBD.setCursor(lugares.extraeCursor());
                        adaptadorLugaresBD.notifyDataSetChanged();
                        actividad.finish();
                    }})
                .setNegativeButton("Cancelar",null)
                .show();
    }

    public void editar (int pos, int codigoSolicitud) {
        Intent editar = new Intent(actividad, EditarEstacionActivity.class);
        editar.putExtra("pos",pos);
        actividad.startActivityForResult(editar, codigoSolicitud);
    }

    public void guardar(int id, Estacion nuevaEstacion) {
        lugares.actualiza(id,nuevaEstacion);
        adaptadorLugaresBD.setCursor(lugares.extraeCursor());
        adaptadorLugaresBD.notifyDataSetChanged();
    }

    public void nuevo(){
        int id = lugares.nuevo();
        GeoPunto posicion =((Aplicacion) actividad.getApplication()).posicionActual;
        if (!posicion.equals(GeoPunto.SIN_POSICION)) {
            Estacion estacion = lugares.elemento(id);
            estacion.setPosicion(posicion);
            lugares.actualiza(id, estacion);
        }
        Intent nueva_estacion = new Intent(actividad, EditarEstacionActivity.class);
        nueva_estacion.putExtra("_id", id);
        actividad.startActivity(nueva_estacion);
    }

    //Intenciones compartir, llamar, verWeb, verMapa
    public void compartir (Estacion estacion){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "Ve esta estación de recarga " + estacion.getNombre() + "-" + estacion.getDireccion());
        actividad.startActivity(intent);
    }

    public void llamar (Estacion estacion){
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + estacion.getTelefono()));
        actividad.startActivity(intent);
    }

    public void verWeb (Estacion estacion){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(estacion.getUrl()));
        actividad.startActivity(intent);
    }

    public void verMapa (Estacion estacion) {
        double latitud = estacion.getPosicion().getLatitud();
        double longitud = estacion.getPosicion().getLatitud();

        Uri uri = estacion.getPosicion() != GeoPunto.SIN_POSICION
                ?Uri.parse("geo:"+latitud+','+longitud+"?z=18&q="+Uri.encode(estacion.getDireccion()))
                :Uri.parse("geo:0,0?q="+Uri.encode(estacion.getDireccion()));

        Log.d("Comprobar ver mapa", uri+" "+ Uri.encode(estacion.getDireccion())+ "\n" + estacion.getPosicion() + "geo punto" + GeoPunto.SIN_POSICION);
        actividad.startActivity(new Intent("android.intent.action.VIEW", uri));


    }

    // Métodos para insertar imagen en la vista
    public void ponerFoto(int pos, String uri, ImageView imageView) {
        Estacion estacion = adaptadorLugaresBD.estacionPosicion(pos);//lugares.elemento(pos);
        Log.d("URI Foto", "URI foto = "+ pos + estacion.getNombre());
        estacion.setFoto(uri);
        visualizarFoto(estacion, imageView);
        actualizaPosEstacion(pos, estacion);
    }

    public void visualizarFoto(Estacion estacion, ImageView imageView) {
        if (estacion.getFoto() != null && !estacion.getFoto().isEmpty()) {

            if(ContextCompat.checkSelfPermission(actividad,
                    Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                imageView.setImageBitmap(reduceBitmap(actividad, estacion.getFoto(), 1024, 1024));
            } else {
                imageView.setImageBitmap(null);
                solicitarPermiso(Manifest.permission.READ_EXTERNAL_STORAGE,
                        "Sin permiso de lectura no es posible mostrar fotos de memoria externa",SOLICITUD_PERMISO_LECTURA,actividad);
            }

        } else {
            imageView.setImageBitmap(null);
        }
    }

    public void importarGaleria (int codigoSolicitud){
        String accion;
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            accion = Intent.ACTION_OPEN_DOCUMENT;
        } else {
            accion = Intent.ACTION_PICK;
        }

        Intent intent =  new Intent(accion, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        actividad.startActivityForResult(intent, codigoSolicitud);
    }

    public Uri tomarFoto(int codigoSolicitud) {
        try {
            Uri uriUltimaFoto;
            File file = File.createTempFile("img_" + (System.currentTimeMillis()/ 1000), ".jpg" ,
                    actividad.getExternalFilesDir(Environment.DIRECTORY_PICTURES));

            if(ContextCompat.checkSelfPermission(actividad,Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_DENIED){
                solicitarPermiso(Manifest.permission.READ_EXTERNAL_STORAGE,
                        "Sin permiso de lectura no es posible abrir camara",SOLICITUD_PERMISO_LECTURA,actividad);
                uriUltimaFoto = Uri.parse(String.valueOf(R.mipmap.icono_app));
            } else if (Build.VERSION.SDK_INT >= 24) {
                uriUltimaFoto = FileProvider.getUriForFile(actividad, "com.grupoz10g13.electrostation.fileProvider", file);
            } else {
                uriUltimaFoto = Uri.fromFile(file);
            }
            Intent intento_tomarFoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intento_tomarFoto.putExtra (MediaStore.EXTRA_OUTPUT, uriUltimaFoto);
            actividad.startActivityForResult(intento_tomarFoto, codigoSolicitud);
            return uriUltimaFoto;

        } catch (IOException ex) {
            Toast.makeText(actividad, "Error al crear fichero de imagen", Toast.LENGTH_LONG).show();
            return null;
        }
    }

    private Bitmap reduceBitmap(Context contexto, String uri, int maxAncho, int maxAlto) {
        try {
            InputStream input = null;
            Uri u = Uri.parse(uri);
            if (u.getScheme().equals("http") || u.getScheme().equals("https")) {
                input = new URL(uri).openStream();
            } else {
                input = contexto.getContentResolver().openInputStream(u);
            }
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            options.inSampleSize = (int) Math.max(
                    Math.ceil(options.outWidth / maxAncho),
                    Math.ceil(options.outHeight / maxAlto));
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(input, null, options);
        } catch (FileNotFoundException e) {
            Toast.makeText(contexto, "Fichero/recurso de imagen no encontrado"+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            Toast.makeText(contexto, "Error accediendo a imagen"+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return null;
        }
    }
}
