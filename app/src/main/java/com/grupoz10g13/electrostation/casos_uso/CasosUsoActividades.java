package com.grupoz10g13.electrostation.casos_uso;

import android.app.Activity;
import android.content.Intent;

import com.grupoz10g13.electrostation.presentacion.AcercaDeActivity;
import com.grupoz10g13.electrostation.presentacion.MapaActivity;
import com.grupoz10g13.electrostation.presentacion.PreferenciasActivity;
import com.grupoz10g13.electrostation.presentacion.UsuarioActivity;

public class CasosUsoActividades {

    protected Activity actividad;

    public CasosUsoActividades(Activity actividad) {
        this.actividad = actividad;
    }

    public void lanzarAcercaDe() {
        actividad.startActivity(new Intent(actividad, AcercaDeActivity.class));
    }

    public void lanzarPreferencias(int codigoSolicitud){
        actividad.startActivityForResult(new Intent(actividad, PreferenciasActivity.class), codigoSolicitud);
    }

    public void lanzarMapa() {
        actividad.startActivity(new Intent(actividad, MapaActivity.class));
    }

    public void lanzarUsuario(){
        actividad.startActivity(new Intent(actividad, UsuarioActivity.class));
    }
}
