package com.grupoz10g13.electrostation.datos;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.grupoz10g13.electrostation.modelo.Usuario;

public class Usuarios {
    public static void guardarUsuario(final FirebaseUser user){
        Usuario usuario = new Usuario(user.getDisplayName(),user.getEmail());
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("usuarios").document(user.getUid()).set(usuario);
    }
}
