package com.grupoz10g13.electrostation.datos;

import com.grupoz10g13.electrostation.modelo.Estacion;

public interface RepositorioLugares {
    Estacion elemento (int id);
    void aniade(Estacion estacion); //Añade el elemento indicado
    int nuevo(); //Añade un elemento en blanco y devuelve su id
    void borrar(int id); //Elimina el elemento con el id indicado
    int tamanio(); //Devuelve el número de elementos
    void actualiza(int id, Estacion estacion); //Reemplaza un elemento
}
