package com.grupoz10g13.electrostation.datos;

import com.grupoz10g13.electrostation.modelo.TipoConector;
import com.grupoz10g13.electrostation.modelo.TipoLocalizacion;
import com.grupoz10g13.electrostation.modelo.Estacion;

import java.util.ArrayList;
import java.util.List;

public class EstacionesLista implements RepositorioLugares {
    protected List<Estacion> listaEstaciones;

    public EstacionesLista(){
        listaEstaciones = new ArrayList<Estacion>();
        aniadeEstacionesEj();
    }

    @Override
    public Estacion elemento(int id) {
        return listaEstaciones.get(id);
    }

    @Override
    public void aniade(Estacion estacion) {
        listaEstaciones.add(estacion);
    }

    @Override
    public int nuevo() {
        Estacion estacion = new Estacion();
        listaEstaciones.add(estacion);
        return listaEstaciones.size()-1;
    }

    @Override
    public void borrar(int id) {
        listaEstaciones.remove(id);
    }

    @Override
    public int tamanio() {
        return listaEstaciones.size();
    }

    @Override
    public void actualiza(int id, Estacion lugar) {
        listaEstaciones.set(id, lugar);
    }

    public void aniadeEstacionesEj(){
        aniade(new Estacion("Galerias", "Cra 24 # 53 - 18", 315234402, "https://www.enel.com.co/es",
                0,6000,"10:00 am - 10:00 pm", "Lavado", "Dentro del parqueradero del centro conmercial",
                "Estacionar en los lugares asigandos", 3, "", 4, -74.0743, 4.6422,
                TipoLocalizacion.MALL, TipoConector.CCS1));

        aniade(new Estacion("Sincromotors", "Cra 9 # 44 - 77", 0, "https://www.sincromotors.com/WebSite/Default.aspx",
                0,0,"7:00 am - 6:00 pm", "Taller", "Dentro del concesionario",
                "Privado, solo vehiculos propios o en mantenimiento", 2, "", 2, -74.0654, 4.6338,
                TipoLocalizacion.CONCESIONARIO, TipoConector.CCS2));

        aniade(new Estacion("Exito Chapinero", "Cll 52 # 13 - 70", 321425698, "https://www.enel.com.co/es",
                0,5000,"7:00 am - 6:00 pm", "Lavado", "Dentro del parqueadero",
                "Estacionar y pedir que habiliten el cargador", 2, "", 5, -74.0653,4.6397,
                TipoLocalizacion.PARKING, TipoConector.CHADEMO));

        aniade(new Estacion("Unicentro ", "Crr 15 # 122 - 18", 2253688, "https://www.enel.com.co/es",
                0,7000,"7:00 am - 6:00 pm", "Lavado", "Dentro del parqueadero",
                "Estacionar en los lugares asigandos", 13, "", 5, -74.0414,4.70220,
                TipoLocalizacion.MALL, TipoConector.GBT1));

        aniade(new Estacion("Terpel Tenjo ", "Tenjo, Cundinamarca", 0, "https://www.terpel.com/en/home-Productos-y-Servicios/Voltex/",
                0,0,"24 horas", "Lavado, tienda conveniencia Altoque", "A 1 KM del peaje Siberia",
                "Estacionar en los lugares asigandos", 2, "", 4, -74.1951,4.7890,
                TipoLocalizacion.ESTACION, TipoConector.GBT2));
    }
}
