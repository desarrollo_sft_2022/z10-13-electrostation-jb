package com.grupoz10g13.electrostation.datos;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.Nullable;

import com.grupoz10g13.electrostation.Aplicacion;
import com.grupoz10g13.electrostation.modelo.Estacion;
import com.grupoz10g13.electrostation.modelo.GeoPunto;
import com.grupoz10g13.electrostation.modelo.TipoConector;
import com.grupoz10g13.electrostation.modelo.TipoLocalizacion;

public class EstacionesBD extends SQLiteOpenHelper implements RepositorioLugares {
    Context contexto;

    public EstacionesBD(Context contexto){
        super(contexto,"estaciones.db",null,1);
        this.contexto = contexto;
    }

    @Override
    public void onCreate(SQLiteDatabase Database) {
        Database.execSQL("CREATE TABLE estaciones (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "nombre TEXT," +
                "direccion TEXT," +
                "telefono BIGINT," +
                "url TEXT," +
                "costoRecarga REAL," +
                "costoParqueo REAL," +
                "horario TEXT," +
                "servicios TEXT," +
                "indicaciones TEXT," +
                "comoFunciona TEXT," +
                "espacios INTEGER," +
                "foto TEXT," +
                "valoracion REAL," +
                "longitud REAL," +
                "latitud REAL," +
                "tipoLocalizacion INTEGER," +
                "tipoConector INTEGER)");

        Database.execSQL("INSERT INTO estaciones VALUES (null," +
                "'Galerias CC'," +
                "'Cra 24 # 53 - 18'," +
                "315234402," +
                "'https://www.enel.com.co/es'," +
                "0," +
                "60," +
                "'9:00 am - 10:00 pm'," +
                "'Lavado'," +
                "'Dentro del parqueradero del centro conmercial'," +
                "'Estacionar en los lugares asigandos'," +
                "3," +
                "''," +
                "4," +
                "-74.0743," +
                "4.6422," +
                TipoLocalizacion.MALL.ordinal() +","+
                TipoConector.CCS1.ordinal()+")");

        Database.execSQL("INSERT INTO estaciones VALUES (null," +
                "'Sincromotors SA'," +
                "'Cra 9 # 44 - 77'," +
                "0," +
                "'https://www.sincromotors.com/WebSite/Default.aspx'," +
                "0," +
                "6" +
                "0," +
                "'7:00 am - 6:00 pm'," +
                "'Taller'," +
                "'Dentro del concesionario'," +
                "'Privado, solo vehiculos propios o en mantenimiento'," +
                "2," +
                "''," +
                "2," +
                "-74.0654," +
                "4.6338," +
                TipoLocalizacion.CONCESIONARIO.ordinal() +","+
                TipoConector.CCS2.ordinal()+")");

        Database.execSQL("INSERT INTO estaciones VALUES (null," +
                "'Exito Chapinero T'," +
                "'Cll 52 # 13 - 70'," +
                "321425698," +
                "'https://www.enel.com.co/es'," +
                "0," +
                "120," +
                "'10:00 am - 10:00 pm'," +
                "'Lavado'," +
                "'Dentro del parqueardero'," +
                "'Estacionar y pedir que habiliten el cargador'," +
                "2," +
                "''," +
                "5," +
                "-74.0653," +
                "4.6397," +
                TipoLocalizacion.TIENDA.ordinal() +","+
                TipoConector.CHADEMO.ordinal()+")");

        Database.execSQL("INSERT INTO estaciones VALUES (null," +
                "'Unicentro CC'," +
                "'Cra 15 # 122 - 18'," +
                "2253688," +
                "'https://www.enel.com.co/es'," +
                "0," +
                "90," +
                "'7:00 am - 6:00 pm'," +
                "'Lavado'," +
                "'Dentro del parqueradero del centro conmercial'," +
                "'Estacionar en los lugares asigandos'," +
                "13," +
                "''," +
                "5," +
                "-74.0414," +
                "4.70220," +
                TipoLocalizacion.MALL.ordinal() +","+
                TipoConector.GBT1.ordinal()+")");

        Database.execSQL("INSERT INTO estaciones VALUES (null," +
                "'Terpel Tenjo ES'," +
                "'Tenjo, Cundinamarca'," +
                "315234402," +
                "'https://www.terpel.com/en/home-Productos-y-Servicios/Voltex/'," +
                "0," +
                "0," +
                "'24 horas'," +
                "'Lavado, tienda conveniencia Altoque'," +
                "'A 1 KM del peaje Siberia, junto a las de combustible'," +
                "'Estacionar en los lugares asigandos'," +
                "2," +
                "''," +
                "4," +
                "-74.1951," +
                "4.7890," +
                TipoLocalizacion.ESTACION.ordinal() +","+
                TipoConector.GBT2.ordinal()+")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    @Override
    public Estacion elemento(int id) {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM estaciones WHERE _id = "+id, null);
        Log.d("TAG","cursor "+ cursor);
        try {
            if (cursor.moveToNext())
                return extraeEstacion(cursor);
            else
                throw new SQLException("Error al acceder al elemento _id = "+id);
        } catch (Exception e) {
            Log.d("TAG","Error elemento lugares db exec "+e.getMessage());
            throw e;
        } finally {
            if (cursor!=null) cursor.close();
        }
    }

    @Override
    public void aniade(Estacion lugar) {

    }

    @Override
    public int nuevo() {
        int _id = -1;
        Estacion estacion = new Estacion();
        String consulta1 = "INSERT INTO estaciones (nombre, direccion, " +
                "telefono, url, costoRecarga, costoParqueo, horario, servicios, " +
                "indicaciones, comoFunciona, espacios, foto, valoracion, longitud, " +
                "latitud, tipoLocalizacion, tipoConector) VALUES("+estacion.getNombre()+",''," +
                "0,'',0,0,'',''," +
                "'','',0,'',0,"+estacion.getPosicion().getLongitud()+","+
                estacion.getPosicion().getLatitud()+","+estacion.getLugar().ordinal()+","+estacion.getConector().ordinal()+")";
        Log.d("consulta1", consulta1);
        getWritableDatabase().execSQL(consulta1);

        String consulta =  "SELECT _id FROM estaciones WHERE nombre = " + estacion.getNombre();
        Log.d("consulta", consulta);
        Cursor c = getReadableDatabase().rawQuery(consulta, null);

        if (c.moveToNext()) _id = c.getInt(0);
        c.close();

        Log.d("tag ID", String.valueOf(_id));
        return _id;
    }

    @Override
    public void borrar(int id) {
        getWritableDatabase().execSQL("DELETE FROM estaciones WHERE _id =" + id);
    }

    @Override
    public int tamanio() {
        return 0;
    }

    @Override
    public void actualiza(int id, Estacion estacion) {
        getWritableDatabase().execSQL("UPDATE estaciones SET" +
                " nombre = '"+ estacion.getNombre() +
                "', direccion = '"+ estacion.getDireccion()+
                "', telefono = "+ estacion.getTelefono() +
                ", url = '"+ estacion.getUrl() +
                "', costoRecarga = "+ estacion.getCostoRecarga() +
                ", costoParqueo = " + estacion.getCostoEstacionamiento() +
                ", horario = '" + estacion.getHorario() +
                "', servicios = '" + estacion.getServiciosAdicionales() +
                "', indicaciones = '" + estacion.getIndicaciones() +
                "', comoFunciona = '" + estacion.getComoFunciona() +
                "', espacios = " + estacion.getEspacios() +
                ", foto = '" + estacion.getFoto() +
                "', valoracion = " + estacion.getValoracion() +
                ", longitud = " + estacion.getPosicion().getLongitud() +
                ", latitud =  " + estacion.getPosicion().getLatitud() +
                ", tipoLocalizacion = " + estacion.getLugar().ordinal() +
                ", tipoConector = " + estacion.getConector().ordinal() +
                " WHERE _id = " + id);
    }

    public static Estacion extraeEstacion(Cursor cursor){
        Estacion estacion = new Estacion();
        estacion.setNombre(cursor.getString(1));
        estacion.setDireccion(cursor.getString(2));
        estacion.setTelefono(cursor.getLong(3));
        estacion.setUrl(cursor.getString(4));
        estacion.setCostoRecarga(cursor.getFloat(5));
        estacion.setCostoEstacionamiento(cursor.getFloat(6));
        estacion.setHorario(cursor.getString(7));
        estacion.setServiciosAdicionales(cursor.getString(8));
        estacion.setIndicaciones(cursor.getString(9));
        estacion.setComoFunciona(cursor.getString(10));
        estacion.setEspacios(cursor.getInt(11));
        estacion.setFoto(cursor.getString(12));
        estacion.setValoracion(cursor.getFloat(13));
        estacion.setPosicion(new GeoPunto(cursor.getDouble(14),cursor.getDouble(15)));
        estacion.setLugar(TipoLocalizacion.values()[cursor.getInt(16)]);
        estacion.setConector(TipoConector.values()[cursor.getInt(17)]);
        return estacion;
    }

    public Cursor extraeCursor() {

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(contexto);
        String consulta, max;

        switch (pref.getString("orden", "0")) {
            case "0":
                double lon = ((Aplicacion) contexto.getApplicationContext()).posicionActual.getLongitud();
                double lat = ((Aplicacion) contexto.getApplicationContext()).posicionActual.getLatitud();
                consulta = "SELECT * FROM estaciones ORDER BY " +
                        "(" + lon + "-longitud)*(" + lon + "-longitud) + " +
                        "(" + lat + "-latitud )*(" + lat + "-latitud )";
                break;
            case "1":
                consulta = "SELECT * FROM estaciones ORDER BY costoRecarga ASC";
                break;
            case "2":
                consulta = "SELECT * FROM estaciones ORDER BY espacios DESC";
                break;
            default:
                consulta = "SELECT * FROM estaciones ORDER BY valoracion DESC";
                break;
        }

        max= pref.getString("maximo","12");
        consulta += " LIMIT "+pref.getString("maximo",max);

        SQLiteDatabase bd = getReadableDatabase();
        return bd.rawQuery(consulta, null);
    }
}
